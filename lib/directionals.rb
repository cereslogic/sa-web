module Directionals
  DIRECTIONAL_MAP = {
    'NORTH' => 'N',
    'N' => 'N',
    'NORTHEAST' => 'NE',
    'NE' => 'NE', 
    'EAST' => 'E',
    'E' => 'E',
    'SOUTHEAST' => 'SE',
    'SE' => 'SE',
    'SOUTH' => 'S',
    'S' => 'S',
    'SOUTHWEST' => 'SW',
    'SW' => 'SW',
    'WEST' => 'W',
    'W' => 'W',
    'NORTHWEST' => 'NW',
    'NW' => 'NW'
  }
end
