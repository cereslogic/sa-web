namespace :one_offs do
  desc "Sets up recharge plans"
  task :populate_plans => :environment do
    Plan.delete_all
    User.find_each do |u|
      u.update_column(:plan_id, nil)
    end

    # $0.05/hit, $20 recharge, 400 credits
    Plan.create(short_description: 'Bronze', description: '$0.05/hit, $20 recharge', recharge_threshold: 40, recharge_fee: 20, recharge_credits: 400, visible_to_users: true, default_at_signup: true)

    # $0.025/hit, $40 recharge, 1600 credits
    Plan.create(short_description: 'Silver', description: '$0.025/hit, $40 recharge', recharge_threshold: 60, recharge_fee: 40, recharge_credits: 1600, visible_to_users: true, default_at_signup: false)

    # $0.01/hit, $80 recharge, 8000 credits
    Plan.create(short_description: 'Gold', description: '$0.01/hit, $80 recharge', recharge_threshold: 100, recharge_fee: 80, recharge_credits: 8000, visible_to_users: true, default_at_signup: false)
  end
end
