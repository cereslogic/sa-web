namespace :loader do
  desc 'Load a file into the search index.'
  task :process_file => :environment do
    File.open(ENV['FILE']).each do |line|
      puts line
      begin
        address = hashify(line)
        Address.create address
      rescue => e
        puts $!
        puts "Exception: #{e.message}"
        puts "Could not process line: #{line}"
      end
    end
  end
end

def self.hashify(line)
  {
    :copyright_detail_code => line[0],
    :zip_code => line[2..6],
    :update_key_no => line[8..17],
    :action_code => line[19],
    :record_type => line[21],
    :carrier_route_id => line[23..26],
    :street_pre_direction_abbrev => line[28].strip,
    :street_name => line[31..59].strip,
    :street_suffix_abbrev => line[60..63].strip,
    :street_post_direction_abbrev => line[64..67].strip,
    :addr_primary_low_number => maybe_an_integer(line[68..77]),
    :addr_primary_high_number => maybe_an_integer(line[79..88]),
    :addr_primary_low_number_string => line[68..77].strip,
    :addr_primary_high_number_string => line[79..88].strip,
    :addr_primary_odd_even_code => line[90],
    :building_or_firm_name => line[92..132].strip,
    :addr_secondary_abbrev => line[133..137].strip,
    :addr_secondary_low_number => maybe_an_integer(line[138..145]),
    :addr_secondary_high_number => maybe_an_integer(line[147..154]),
    :addr_secondary_low_number_string => line[138..145].strip,
    :addr_secondary_high_number_string => line[147..154].strip,
    :addr_secondary_odd_even_code => line[156],
    :zip_add_on_low_number_sector_number => line[158..159],
    :zip_add_on_low_number_segment_number => line[160..161],
    :zip_add_on_high_number_sector_number => line[163..164],
    :zip_add_on_high_number_segment_number => line[165..166],
    :base_alt_code => line[168],
    :lacs_status_ind => line[170],
    :govt_building_indicator => line[172],
    :finance_number => line[174..179],
    :state_abbrev => line[181..182],
    :county_number => line[184..186],
    :city_name => line[188..215].strip,
    :congressional_district_number => line[217..218],
    :municipality_city_state_key => line[220..225],
    :urbanization_city_state_key => line[227..232],
    :preferred_last_line_city_state_key => line[234..239],
    :from_latitude => line[241..250],
    :from_longitude => line[252..262],
    :to_latitude => line[264..273],
    :to_longitude => line[275..285]
  }
end

def self.maybe_an_integer(string)
  string.strip.match(/^\d+$/) ? string.strip.to_i : nil
end

  
