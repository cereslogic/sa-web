namespace :periodic do
  desc "Creates usage history records every night."
  task :nightly_usage_snapshot => :environment do
    User.find_each do |u|
      usage_today = u.usage_today.getset(0)
      u.daily_usages.create!({day: Date.today, api_calls: usage_today})
    end
  end
end
