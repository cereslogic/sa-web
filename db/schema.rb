# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150423233820) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "daily_usages", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid     "user_id"
    t.integer  "api_calls"
    t.date     "day"
  end

  create_table "identities", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid     "user_id"
    t.string   "provider"
    t.string   "uid"
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "payments", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "amount_cents",    default: 0,     null: false
    t.string   "amount_currency", default: "USD", null: false
    t.integer  "credits"
    t.uuid     "user_id"
    t.string   "description"
  end

  create_table "plans", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "short_description"
    t.string   "description"
    t.integer  "recharge_threshold"
    t.integer  "recharge_credits"
    t.integer  "recharge_fee_cents",    default: 0,     null: false
    t.string   "recharge_fee_currency", default: "USD", null: false
    t.boolean  "default_at_signup"
    t.boolean  "visible_to_users"
  end

  create_table "users", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "stripe_token"
    t.string   "stripe_customer_id"
    t.string   "card_last4"
    t.integer  "card_exp_year"
    t.integer  "card_exp_month"
    t.string   "card_brand"
    t.uuid     "plan_id"
    t.string   "password_reset_token"
    t.datetime "password_reset_at"
  end

  add_index "users", ["password_reset_token"], name: "index_users_on_password_reset_token", using: :btree

end
