class AddMoreColumnsToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :default_at_signup, :boolean
    add_column :plans, :visible_to_users, :boolean
  end
end
