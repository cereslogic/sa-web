class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments, id: :uuid do |t|
      t.timestamps
      t.monetize    :amount
      t.integer     :credits
      t.uuid        :user_id
      t.string      :description
    end
  end
end
