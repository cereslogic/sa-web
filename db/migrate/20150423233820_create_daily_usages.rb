class CreateDailyUsages < ActiveRecord::Migration
  def change
    create_table :daily_usages, id: :uuid do |t|
      t.timestamps null: false
      t.uuid    :user_id
      t.integer :api_calls
      t.date    :day
    end
  end
end
