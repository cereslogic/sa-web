class CreatePlansTable < ActiveRecord::Migration
  def change
    create_table :plans, id: :uuid do |t|
      t.timestamps null: false
      t.string     :short_description
      t.string     :description
      t.integer    :recharge_threshold
      t.integer    :recharge_credits
      t.monetize   :recharge_fee
    end

    add_column :users, :plan_id, :uuid
  end
end
