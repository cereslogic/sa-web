class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users, id: :uuid do |t|
      t.timestamps null: false
      t.string :name
      t.string :email
      t.string :password_digest
    end
  end
end
