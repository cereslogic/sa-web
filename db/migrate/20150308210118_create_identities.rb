class CreateIdentities < ActiveRecord::Migration
  def change
    create_table :identities, id: :uuid do |t|
      t.timestamps null: false
      t.uuid       :user_id
      t.string     :provider
      t.string     :uid
    end

    add_index :identities, :user_id
  end
end
