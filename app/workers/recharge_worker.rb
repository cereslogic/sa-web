class RechargeWorker 
  include Sidekiq::Worker

  # Needs a spec!
  def perform(user_id)
    user = User.find(user_id)
    plan = user.plan
    return if plan.nil?  # TODO: send e-mail here?

    if user.credits_remaining.value == user.recharge_threshold.to_i
      Stripe::Charge.create(
        amount: plan.recharge_fee_cents,
        currency: 'usd',
        customer: user.stripe_customer_id
      )

      user.credits_remaining.increment(user.plan.recharge_credits)
      user.payments << Payment.new(
                         amount_cents: plan.recharge_fee_cents, 
                         credits: plan.recharge_credits, 
                         description: "#{plan.short_description} plan: Recharge of #{plan.recharge_credits} credits"
                       )
      user.save

      PaymentsMailer.recharged(user.id, plan.recharge_fee_cents, plan.recharge_credits, plan.short_description).deliver
    end
  end
end
