class PasswordReset
  def initialize(user)
    @user = user
  end

  def send_password_reset
    @user.password_reset_token = SecureRandom.urlsafe_base64
    @user.password_reset_at = DateTime.now
    @user.save!
    UserMailer.password_reset(@user.id).deliver
  end
end
