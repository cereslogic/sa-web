class PaymentService
  def self.stripe_customer
    Stripe::Customer
  end

  def self.sync_with_stripe(user)
    if user.stripe_customer_id.present?
      customer = stripe_customer.retrieve(user.stripe_customer_id)
      if user.stripe_token == nil
        # Removing customer/card
        customer.delete if customer.present? # && !customer.deleted
        user.reset_payment_fields
      else
        # Updating customer/card
        customer.card = user.stripe_token
        customer.save
        user.stripe_customer_id = customer.id
      end
    else
      # New customer/card
      customer = stripe_customer.create(
                   card: user.stripe_token,
                   description: user.name,
                   email: user.email
                 )
      user.stripe_customer_id = customer.id
    end
  end
end
