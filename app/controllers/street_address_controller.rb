class StreetAddressController < ApplicationController
  skip_before_filter :verify_authenticity_token

  rescue_from Sentry::SentryError, with: :sentry_error

  # input_id      string  16  A unique identifier for this address used in your application; this field will be copied into the output
  # street        string  64  Required. The street line of the address, or an entire address
  # street2       string  64  Any extra address information
  # secondary     string  32  Apartment, suite, or office number
  # city          string  64  The city name
  # state         string  32  The state name or abbreviation
  # zipcode       string  16  The ZIP Code
  # lastline      string  64  City, state, and ZIP Code combined
  # addressee	    string  64  The name of the recipient, firm, or company at this address
  # urbanization	string  64  Only used with Puerto Rico
  # candidates    integer     Max Value: 10 The maximum number of valid addresses returned when the input is ambiguous.
  # auth-id       string      User ID
  # auth-token    string      Authorization token
  #
  def query
    Sentry.gate(params, request.env)
    
    @delivery_line = DeliveryLineParser.new(params[:street], params[:street2] || '')
    @query         = AddressQuery.new(params, @delivery_line)
    @found         = Address.search(@query.to_query)
    @results       = AddressSerializer.new(@delivery_line, @query, @found).to_hash
    respond_to do |format|
      format.any(:json, :v1_api) { render json: @results }
    end
  end

  def sentry_error(exception)
    head(exception.status_code)
  end
end
