class PasswordResetsController < ApplicationController
  def new
    render_404 and return if current_user.present?
    render_404 and return unless @user = User.find_by(password_reset_token: params[:t])
  end

  def create
    render_404 and return if current_user.present?
    user = User.find_by(email: params[:email])
    if user
      PasswordReset.new(user).send_password_reset
    else
      flash[:error] = "Sorry, we don't have that email address on file."
      redirect_to new_password_reset_path
    end
  end
end
