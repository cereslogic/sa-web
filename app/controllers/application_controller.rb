class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  protected

  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  def signed_in?
    !!current_user
  end
  helper_method :current_user, :signed_in?

  def current_user=(user)
    @current_user = user
    session[:user_id] = user.nil? ? nil : user.id
  end

  def authenticate
    redirect_to login_path unless signed_in?
  end

  def render_401
    respond_to do |format|
      format.any { head :unauthorized }
    end
    return true
  end

  def render_403
    respond_to do |format|
      format.any { head :forbidden }
    end
    return true
  end

  def render_402
    respond_to do |format|
      format.any { head :payment_required }
    end
    return true
  end

  def render_404
    raise ActionController::RoutingError.new('Not Found')
  end
end
