class AuthTokensController < ApplicationController
  def create
    current_user.auth_tokens << params[:new_auth_token]
    current_user.auth_token_descriptions[params[:new_auth_token]] = params[:description]
    if current_user.save
      flash[:notice] = 'New authorization token created.'
    else
      flash[:error] = "Unable to create new authorization token: #{current_user.errors.full_messages.to_sentence}"
    end
    redirect_to user_path(current_user)
  end

  def destroy
    current_user.auth_tokens.delete(params[:id])
    current_user.auth_token_descriptions.delete(params[:id])
    if current_user.save
      flash[:notice] = 'Authorization token deleted.'
    else
      flash[:error] = "Unable to delete authorization token: #{current_user.errors.full_messages.to_sentence}"
    end
    redirect_to user_path(current_user)
  end
end
