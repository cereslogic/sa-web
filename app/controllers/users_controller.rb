class UsersController < ApplicationController
  before_filter :authenticate, only: [:show]

  def show
    @payments = current_user.payments.order("created_at DESC").page(params[:page]).per(5)
    @history = DailyUsage.history(current_user)
  end

  def new
    @user = env['omniauth.identity'] ||= User.new
  end

  def update
    if current_user.update_attributes(params.require(:user).permit(:plan_id))
      redirect_to user_path(current_user), notice: "Account settings updated."
    else
      redirect_to user_path(current_user), notice: "Error updating plan: #{current_user.errors.full_messages.to_sentence}"
    end      
  end
end
