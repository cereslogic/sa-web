class PasswordsController < ApplicationController
  # Came in from a password reset
  def new
    # render_404 and return if current_user.present?
    render_404 and return unless params[:t].present?
    user = User.find_by(password_reset_token: params[:t])
    self.current_user = user
  end

  # Came in from a user changing her/his password.
  def edit
    render_404 and return if current_user.nil?
  end

  def update
    render_404 and return if current_user.nil?
    if current_user.authenticate(params[:current_password]) || current_user.password_reset_token == params[:password_reset_token]
      current_user.password = params[:password]
      current_user.password_confirmation = params[:confirm_password]
      if current_user.save
        flash[:notice] = "Your password has been updated."
        redirect_to user_path(current_user)
      else
        flash[:error] = "There was an error updating your password: #{current_user.errors.full_messages.to_sentence}"
        redirect_to :back
      end
    else
      flash[:error] = "What you provided as your current password does not match our records."
      redirect_to :back
    end
  end
end
