class PaymentsController < ApplicationController
  before_filter :authenticate

  def index
    @payments = current_user.payments.order("created_at DESC").page(params[:page]).per(5)
  end
end
