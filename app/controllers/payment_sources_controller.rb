class PaymentSourcesController < ApplicationController
  before_filter :authenticate, only: [:show]

  def create
    current_user.stripe_token = params[:token][:id]
    current_user.card_last4 = params[:token][:card][:last4]
    current_user.card_exp_year = params[:token][:card][:exp_year]
    current_user.card_exp_month = params[:token][:card][:exp_month]
    current_user.card_brand = params[:token][:card][:brand]

    if current_user.save
      redirect_to user_path(current_user), notice: 'Payment source has been updated.'
    else
      redirect to user_path(current_user), error: 'Error updating payment source.'
    end
  end

  def destroy
    current_user.stripe_token = nil
    if current_user.save
      redirect_to user_path(current_user), notice: 'Payment source has been removed.'
    else
      redirect to user_path(current_user), error: 'Error removing payment source.'
    end      
  end
end
