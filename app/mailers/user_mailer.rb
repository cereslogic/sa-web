class UserMailer < ApplicationMailer
  def welcome(user_id)
    @user = User.find(user_id)
    mail(to: @user.email, subject: "Welcome to Sure Address!")
  end

  def password_reset(user_id)
    @user = User.find(user_id)
    mail(to: @user.email, subject: "Password Reset Instructions")
  end
end
