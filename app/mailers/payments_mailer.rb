class PaymentsMailer < ApplicationMailer
  def recharged(user_id, recharge_fee_cents, recharge_credits, plan_short_description)
    @user = User.find(user_id)
    @recharge_fee_cents = recharge_fee_cents
    @recharge_credits = recharge_credits
    @plan_name = plan_short_description
    mail(to: @user.email, subject: "Account recharged: $#{Money.new(recharge_fee_cents)}, added #{recharge_credits} credits")
  end
end
