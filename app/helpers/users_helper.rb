module UsersHelper
  def token_description(user, token)
    current_user.auth_token_descriptions[token].blank? ? 'Default Token' : current_user.auth_token_descriptions[token]
  end
end
