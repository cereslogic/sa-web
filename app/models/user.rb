# from https://gist.github.com/stefanobernardi/3769177
class User < OmniAuth::Identity::Models::ActiveRecord
  include Redis::Objects

  has_many   :identities
  has_many   :payments
  has_many   :daily_usages
  belongs_to :plan, autosave: true

  before_save :update_stripe, :if => Proc.new { |u| u.stripe_token_changed? }
  before_create :assign_default_plan_at_signup
  after_create :add_first_auth_token
  after_commit :send_welcome_email, on: :create
  after_commit :first_recharge

  # TODO
  # email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  # validates :email, :presence   => true,
  #           :format     => { :with => email_regex },
  #           :uniqueness => { :case_sensitive => false }
  
  list :auth_tokens
  hash_key :auth_token_descriptions
  value :recharge_threshold

  counter :credits_remaining
  counter :usage_today

  def self.create_with_omniauth(auth)
    # you should handle here different providers data
    # eg. case auth['provider'] ..
    create(name: auth['info']['name'])
    # IMPORTANT: when you're creating a user from a strategy that
    # is not identity, you need to set a password, otherwise it will fail
    # I use: user.password = rand(36**10).to_s(36)
  end

  def has_payment_source?
    self.stripe_token.present?
  end

  def reset_payment_fields
    self.stripe_token = nil
    self.stripe_customer_id = nil
    self.card_last4 = nil
    self.card_exp_year = nil
    self.card_exp_month = nil
    self.card_brand = nil
  end

  def generate_new_token
    SecureRandom.uuid
  end

  protected

  def update_stripe
    PaymentService.sync_with_stripe(user)
  end

  def send_welcome_email
    UserMailer.welcome(self.id).deliver
  end

  def assign_default_plan_at_signup
    self.plan = Plan.default_at_signup.first
  end

  def add_first_auth_token
    uuid = self.generate_new_token
    self.auth_tokens << uuid
    self.auth_token_descriptions[uuid] = ''
  end

  def first_recharge
    RechargeWorker.perform_async(self.id)
  end
end
