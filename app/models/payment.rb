class Payment < ActiveRecord::Base
  belongs_to :user

  monetize :amount_cents

  # scope :for_user, -> { |u| where(user: user).order('created_at DESC') }
end
