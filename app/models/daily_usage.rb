class DailyUsage < ActiveRecord::Base
  belongs_to :user

  def self.history(user, number_of_days=7)
    result = {}

    start = number_of_days.days.ago.to_date
    finish = Date.today
    (start..finish).each { |day| result[day.strftime('%b %-d')] = 0 }
    self.where(user: user).where('day >= ?', start).each do |usage|
      result[usage.day.strftime('%b %-d')] = usage.api_calls
    end
    
    result.to_a
  end
end
