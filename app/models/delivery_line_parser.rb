require 'street_suffixes'
require 'secondary_unit_designators'
require 'directionals'

class DeliveryLineParser
  include StreetSuffixes
  include SecondaryUnitDesignators
  include Directionals

  attr_accessor :street, :street2
  attr_accessor :assigned_fields
  attr_accessor :unassigned_fields
  attr_accessor :field_numbers

  # See http://pe.usps.gov/text/pub28/28c2_012.htm
  #
  # 101 MAIN ST
  # 101 MAIN ST APT 12
  # 101 W MAIN ST APT 12
  # 101 W MAIN ST S APT 12
  #
  attr_accessor :primary_address_number
  attr_accessor :predirectional
  attr_accessor :street_name
  attr_accessor :suffix
  attr_accessor :postdirectional
  attr_accessor :secondary_address_identifier
  attr_accessor :secondary_address

  ORDINALS = {
    'FIRST' => '1',
    '1ST' => '1',
    'SECOND' => '2',
    '2ND' => '2',
    'THIRD' => '3',
    '3RD' => '3',
    'FOURTH' => '4',
    '4TH' => '4',
    'FIFTH' => '5',
    '5TH' => '5',
    'SIXTH' => '6',
    '6TH' => '6',
    'SEVENTH' => '7',
    '7TH' => '7',
    'EIGHTH' => '8',
    '8TH' => '8',
    'NINTH' => '9',
    '9TH' => '9',
    'TENTH' => '10',
    '10TH' => '11',
  }

  def initialize(street, street2)
    @street = street
    @street2 = street2

    @fields  = @street.split(' ')
    @fields2 = @street2.split(' ')

    possibly_move_street2_to_street

    @assigned_fields = Array.new(@fields.length)
    @unassigned_fields = @fields.clone
    @field_numbers = {}

    @primary_address_number = nil
    @predirectional = nil
    @street_name = nil
    @suffix = nil
    @postdirectional = nil
    @secondary_address_indentifier = nil
    @secondary_address = nil
    parse
  end

  def parse
    @primary_address_number = parse_primary_address_number
    @suffix = parse_suffix
    @predirectional = parse_predirectional
    @secondary_address_identifier = parse_secondary_address_identifier
    @secondary_address = parse_secondary_address
    @street_name = parse_street_name
  end

  protected

  def possibly_move_street2_to_street
    # if it looks like street2 starts off with a secondary address
    # value, then it really belongs on street, so move it.
    return if @fields2.nil? || @fields2.length < 2

    check = @fields2.first.upcase.strip
    if SECONDARY_UNIT_DESIGNATOR_MAP.keys.include?(check)
      @street += " #{@fields2[0]} #{@fields2[1]}"
      @street2 = @fields2[2..-1].join(' ')
      @fields  = @street.split(' ')
      @fields2 = @street2.split(' ')
    end
  end

  def parse_street_name
    @unassigned_fields.compact.map(&:upcase).join(' OR ')
  end

  def parse_predirectional
    primary_address_number_field_number = @field_numbers[:primary_address_number]
    suffix_field_number = @field_numbers[:suffix]

    if primary_address_number_field_number && suffix_field_number
      # The predirectional will come right after the address number. Need to make sure we
      # don't get confused with a street name (e.g., 123 NORTH ST). If there's only one
      # word between the address number and the suffix, there's no predirectional.
      if (suffix_field_number - primary_address_number_field_number) > 2
        predirectional_field_number = primary_address_number_field_number + 1
        check = @fields[predirectional_field_number]
        if DIRECTIONAL_MAP.keys.include?(check.upcase.strip)
          assign_field(check, :predirectional)
          return DIRECTIONAL_MAP[check.upcase.strip]
        end
      end
    end
    nil
  end

  def parse_primary_address_number
    if match = @street.scan(/\d+/).first
      assign_field(match, :primary_address_number)
      return match
    end
    nil
  end

  def parse_suffix
    @fields.reverse_each do |field|
      check = field.upcase.strip
      if STREET_SUFFIX_MAP.keys.include?(check)
        assign_field(field, :suffix)
        return STREET_SUFFIX_MAP[check]
      end
    end
    nil
  end

  def parse_secondary_address_identifier
    # If it's too short, there's likely not a secondary unit designator. We're
    # only going to check the last two fields and only if there are more than
    # three fields. (This might not be good enough if there are directionals 
    # in the address?)
    return nil if @fields.length < 4 and @fields2.empty?
    
    @fields.last(2).reverse_each do |field|
      check = field.upcase.strip
      if SECONDARY_UNIT_DESIGNATOR_MAP.keys.include?(check)
        assign_field(field, :secondary_address_identifier)
        return SECONDARY_UNIT_DESIGNATOR_MAP[check]
      end
    end
    
    nil
  end

  def parse_secondary_address
    return nil if @secondary_address_identifier.nil?
    return nil unless SECONDARY_UNIT_DESIGNATORS_WITH_ADDRESSES.include?(@secondary_address_identifier)
    
    # The secondary address number will likely be on either side of
    # the address identifier. E.g., you might have APT 6 for FOURTH FLOOR.
    # There might also be pound signs involved. -_-
    #
    secondary_address_number_candidate = nil
    if @assigned_fields.last == :secondary_address_identifier  
      # The last field was an identifier (APT, FLR, BLDG, etc), so the
      # address portion should be the field just before it (this is
      # the an unusual case)
      secondary_address_number_candidate = @fields[-2] if @fields[-2] != @suffix
    elsif @assigned_fields[-2] == :secondary_address_identifier
      # The penultimate field is the identifier. So hopefully
      # the last field is an address.
      secondary_address_number_candidate = @fields.last
    end

    if !secondary_address_number_candidate.nil?
      @secondary_address = convert_ordinal(secondary_address_number_candidate)
      assign_field(secondary_address_number_candidate, :secondary_address)
      return @secondary_address
    end

    nil
  end

  def convert_ordinal(value)
    return ORDINALS[value.upcase] if ORDINALS.has_key?(value.upcase)
    value.upcase
  end

  def assign_field(matched_value, assignment)
    field_number = @fields.find_index { |field| Regexp.new(matched_value) =~ field }
    @assigned_fields[field_number] = assignment
    @unassigned_fields[field_number] = nil
    @field_numbers[assignment] = field_number
  end
end
