class AddressSerializer
  def initialize(delivery_line,query,addresses)
    @delivery_line = delivery_line
    @query = query
    @addresses = addresses
  end

  def to_hash
    result = []
    @addresses.each_with_index do |address,i|
      result << {
        :input_index => 0,
        :candidate_index => i,
        :addressee => @query.addressee.try(:upcase) || '',
        :delivery_line_1 => address.delivery_line(@delivery_line.primary_address_number, @delivery_line.secondary_address),
        :delivery_line_2 => @query.street2.try(:upcase),
        :last_line => address.last_line,
        :delivery_point_barcode => '',
        :components => {
          :primary_number => @delivery_line.primary_address_number,
          :street_name => address.street_name.strip,
          :street_suffix => address.street_suffix_abbrev,
          :city_name => address.city_name.strip,
          :state_abbreviation => address.state_abbrev,
          :zipcode => address.zip_code,
          :plus4_code => address.plus4_code,
          :delivery_point => '',
          :delivery_point_check_digit => '',
          :secondary_address_identifier => address.addr_secondary_abbrev,
          :secondary_address => @delivery_line.secondary_address || ''
        },
        :metadata => {
          :record_type => address.record_type,
          :county_fips => address.county_number,
          :county_name => '',
          :carrier_route => address.carrier_route_id,
          :congressional_district => address.congressional_district_number,
          :rdi => "",
          :latitude => (address.from_latitude.to_f + address.to_latitude.to_f)/2.0,
          :longitude => (address.from_longitude.to_f + address.to_longitude.to_f)/2.0,
          :precision => "",
          :key => address.update_key_no
        }
        # 		"analysis": {
        # 			"dpv_match_code": "Y",
        # 			"dpv_footnotes": "AABB",
        # 			"dpv_cmra": "N",
        # 			"dpv_vacant": "N",
        # 			"active": "Y"
        # 		}
      }
    end
    result
  end
end
