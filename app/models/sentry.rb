class Sentry
  class SentryError < StandardError; end
  class OutOfCredits < SentryError; 
    def status_code; :payment_required; end
  end

  class BadCredentials < SentryError
    def status_code; :unauthorized; end
  end

  class MissingCredentials < SentryError; 
    def status_code; :unauthorized; end
  end

  def self.gate(params,headers)
    raise MissingCredentials unless (auth_id = (params['auth-id'] || headers['HTTP_X_SA_AUTH_ID']))
    raise MissingCredentials unless (auth_token = (params['auth-token'] || headers['HTTP_X_SA_AUTH_TOKEN']))

    user = User.new(id: auth_id)
    raise BadCredentials if user.id.nil?

    raise BadCredentials unless user.auth_tokens.include? auth_token
    user.credits_remaining.decrement do |val|
      raise OutOfCredits if val == 0

      if user.credits_remaining == user.recharge_threshold.to_i
        RechargeWorker.perform_async(user.id)
      end

      user.usage_today.increment(1)

      true
    end
  end
end
