class Plan < ActiveRecord::Base
  has_many :users

  validates_presence_of :short_description
  validates_presence_of :description
  
  validates_presence_of :recharge_threshold
  validates_presence_of :recharge_credits
  validates_presence_of :recharge_fee

  validates_numericality_of :recharge_threshold
  validates_numericality_of :recharge_credits

  monetize :recharge_fee_cents

  scope :default_at_signup, -> { where(default_at_signup: true) }
  scope :visible_to_users, -> { where(visible_to_users: true) }
end
