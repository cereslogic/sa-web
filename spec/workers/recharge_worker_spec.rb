# require 'rails_helper'
require 'sidekiq/testing'
require 'stripe'
require 'action_mailer'
require 'monetize'

require_relative '../../app/mailers/application_mailer'
require_relative '../../app/mailers/payments_mailer'
require_relative '../support/mock_models.rb'
require_relative '../../app/workers/recharge_worker'

require 'active_support/core_ext/object'   # for present?

Sidekiq::Testing.inline!

describe RechargeWorker do
  let(:counter) { double('RedisCounter', value: 13) }
  let(:plan)    { double('Plan', recharge_credits: 1000, recharge_fee_cents: 1000, short_description: 'Plan Name') }
  let(:user)    { double('User', plan: plan, recharge_threshold: 13) }
  let(:mail)    { double('Mail') }
  let(:payment) { double('Payment') }

  before do
    allow(User).to receive(:find).and_return(user)
    allow(PaymentsMailer).to receive(:recharged).and_return(mail)
    allow(Stripe::Charge).to receive(:create)
    allow(user).to receive(:credits_remaining).and_return(counter)
    user.as_null_object
    mail.as_null_object
    counter.as_null_object
  end

  context "happy path" do
    it 'should charge stripe' do
      expect(Stripe::Charge).to receive(:create)
      RechargeWorker.new.perform(1)
    end

    it 'should send a notification' do
      expect(PaymentsMailer).to receive(:recharged)
      RechargeWorker.new.perform(1)
    end
    
    it 'should create a payments record' do
      expect(Payment).to receive(:new).and_return(payment)
      expect(user).to receive(:<<).with payment
      expect(user).to receive(:save)
      RechargeWorker.new.perform(1)    
    end

    it 'should increment the user credits' do
      expect(counter).to receive(:increment).with(plan.recharge_credits)
      RechargeWorker.new.perform(1)
    end
  end
end
