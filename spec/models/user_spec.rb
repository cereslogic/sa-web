require 'rails_helper'

RSpec.describe User, :type => :model do
  let(:mail) { double('Mail') }
  let(:plan) { FactoryGirl.build(:plan) }

  before do
    allow(mail).to receive(:deliver)
    allow(UserMailer).to receive(:welcome).and_return(mail)
    allow(Plan).to receive(:default_at_signup).and_return([plan])
  end

  describe '#add_first_auth_token' do
    it 'should add a default auth token when created' do
      user = User.new(password: 'elvisrulez')
      user.save!
      user.reload
      expect(user.auth_tokens.size).to eq 1
      expect(user.auth_token_descriptions.size).to eq 1
    end
  end

  describe '#send_welcome_email' do
    it 'should send a welcome email on account creation' do
      expect(UserMailer).to receive(:welcome)
      expect(mail).to receive(:deliver)
      user = User.new(password: 'elvisrulez')
      user.save!
    end
  end

  describe '#assign_default_plan_at_signup' do
    it 'should set a default plan' do
      user = User.new(password: 'elvisrulez')
      user.save!
      expect(user.plan.id).to eq plan.id
    end
  end
end
