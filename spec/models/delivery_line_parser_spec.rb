# require 'rails_helper'
require_relative '../../app/models/delivery_line_parser'

describe DeliveryLineParser do
  describe "#parse_primary_address_number" do
    it "should find 104 in '104 Shepherd Ln'" do
      dlp = DeliveryLineParser.new('104 Shepherd Ln','')
      expect(dlp.primary_address_number).to eq '104'
      expect(dlp.assigned_fields[0]).to eq :primary_address_number
    end

    it "should find 104 in '104E Shepherd Ln'" do
      dlp = DeliveryLineParser.new('104E Shepherd Ln','')
      expect(dlp.primary_address_number).to eq '104'
      expect(dlp.assigned_fields[0]).to eq :primary_address_number
    end

    it "should find 104 in 'A104 Shepherd Ln'" do
      dlp = DeliveryLineParser.new('A104 Shepherd Ln','')
      expect(dlp.primary_address_number).to eq '104'
      expect(dlp.assigned_fields[0]).to eq :primary_address_number
    end

    it "should find 104 in 'W 104 Shepherd Ln'" do
      dlp = DeliveryLineParser.new('W 104 Shepherd Ln','')
      expect(dlp.primary_address_number).to eq '104'
      expect(dlp.assigned_fields[1]).to eq :primary_address_number
    end

    it "should return nil on 'General Delivery'" do
      dlp = DeliveryLineParser.new('General Delivery','')
      expect(dlp.primary_address_number).to be_nil
    end
  end

  describe "#parse_suffix" do
    it "should find it when it's obvious" do
      dlp = DeliveryLineParser.new('104 shepherd ln','')      
      expect(dlp.suffix).to eq 'LN'
      expect(dlp.assigned_fields[2]).to eq :suffix
    end

    it "should use substitution" do
      dlp = DeliveryLineParser.new('104 shepherd lane','')      
      expect(dlp.suffix).to eq 'LN'
      expect(dlp.assigned_fields[2]).to eq :suffix
    end

    it "shouldn't get confused by suffix-like words appearing in the street name" do
      dlp = DeliveryLineParser.new('104 lane rd','')      
      expect(dlp.suffix).to eq 'RD'
      expect(dlp.assigned_fields[2]).to eq :suffix
    end

    it "shouldn't get confused when it's not at the end" do
      dlp = DeliveryLineParser.new('104 shepherd lane apt 123','')
      expect(dlp.suffix).to eq 'LN'      
      expect(dlp.assigned_fields[2]).to eq :suffix
    end

    it "shouldn't blow up if it's not there" do
      dlp = DeliveryLineParser.new('104 shepherd','')
      expect(dlp.suffix).to be_nil
    end
  end

  describe '#parse_secondary_address_identifier' do
    it 'should work in the easy case' do
      dlp = DeliveryLineParser.new('104 shepherd lane apt 123','')
      expect(dlp.secondary_address_identifier).to eq 'APT'
      expect(dlp.assigned_fields[3]).to eq :secondary_address_identifier
    end

    it 'should work with substitutions' do
      dlp = DeliveryLineParser.new('104 shepherd lane building 123','')
      expect(dlp.secondary_address_identifier).to eq 'BLDG'
      expect(dlp.assigned_fields[3]).to eq :secondary_address_identifier
    end
    
    it 'should work with no address portion' do
      dlp = DeliveryLineParser.new('104 shepherd lane lobby','')
      expect(dlp.secondary_address_identifier).to eq 'LBBY'
      expect(dlp.assigned_fields[3]).to eq :secondary_address_identifier
    end

    it 'should not identify a street name as a secondary designator' do
      dlp = DeliveryLineParser.new('104 building st','')
      expect(dlp.secondary_address_identifier).to be_nil
      expect(dlp.assigned_fields[1]).not_to eq :secondary_address_identifier
    end

    it 'should not identify a street name as a secondary designator even with directionals' do
      dlp = DeliveryLineParser.new('104 w building st s','')
      expect(dlp.secondary_address_identifier).to be_nil
      expect(dlp.assigned_fields[1]).not_to eq :secondary_address_identifier
    end

    it 'should still find a secondary designator even with street names that can be misidentified' do
      dlp = DeliveryLineParser.new('104 w building st s apt c','')
      expect(dlp.secondary_address_identifier).to eq 'APT'
      expect(dlp.assigned_fields[5]).to eq :secondary_address_identifier
    end

    it "finds it if it's in the second line of address" do
      dlp = DeliveryLineParser.new('104 shepherd ln','floor 2')
      expect(dlp.secondary_address_identifier).to eq 'FL'
    end

    it "works with predirectionals (like NYC)" do
      dlp = DeliveryLineParser.new('55 e 72nd st apt 2a','')
      expect(dlp.secondary_address_identifier).to eq 'APT'
    end
  end

  describe '#parse_secondary_address' do
    it 'should work in the easy case' do
      dlp = DeliveryLineParser.new('104 shepherd lane apt 123b','')
      expect(dlp.secondary_address).to eq '123B'
      expect(dlp.assigned_fields[4]).to eq :secondary_address
    end

    it 'should work with ordinal substitutions (1)' do
      dlp = DeliveryLineParser.new('104 shepherd lane fourth floor','')
      expect(dlp.secondary_address).to eq '4'
      expect(dlp.assigned_fields[3]).to eq :secondary_address
    end
    
    it 'should work with ordinal substitutions (2)' do
      dlp = DeliveryLineParser.new('104 shepherd ln 5th floor','')
      expect(dlp.secondary_address).to eq '5'
      expect(dlp.assigned_fields[3]).to eq :secondary_address
    end
  end

  describe "#parse_predirectional" do
    it 'should work with a NY city address' do
      dlp = DeliveryLineParser.new('155 e 72nd street apt 2a','')
      expect(dlp.predirectional).to eq 'E'
      expect(dlp.assigned_fields[1]).to eq :predirectional
    end

    it 'should not call a street name that matches a directional a pre-directional' do
      dlp = DeliveryLineParser.new('155 north st','')
      expect(dlp.predirectional).to eq nil
      expect(dlp.assigned_fields[1]).not_to eq :predirectional
    end
  end

  describe '#parse_street_name' do
    it 'should work in an easy case' do
      dlp = DeliveryLineParser.new('104 shepherd lane','')
      expect(dlp.street_name).to eq 'SHEPHERD'
    end
  end

  describe '#possibly_move_street2_to_street' do
    it 'should work' do
      dlp = DeliveryLineParser.new('104 shepherd lane',"apartment 2a elvis's place")
      expect(dlp.secondary_address_identifier).to eq 'APT'
      expect(dlp.secondary_address).to eq '2A'
      expect(dlp.assigned_fields[3]).to eq :secondary_address_identifier
      expect(dlp.assigned_fields[4]).to eq :secondary_address
    end
  end
end
