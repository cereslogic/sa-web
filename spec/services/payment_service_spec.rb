# require 'rails_helper'
require_relative '../../app/services/payment_service'
require 'active_support/core_ext/object'   # for present?

describe PaymentService do
  let(:stripe_customer_api) { double('StripeCustomerApi') }
  let(:stripe_customer) { double('StripeCustomer', id: '555') }
  let(:user_with_card) { double('UserWithCard', stripe_customer_id: '123', name: 'Elvis', email: 'elvis@graceland.org') }
  let(:user_without_card) { double('UserWithoutCard', stripe_customer_id: nil, name: 'Priscilla', email: 'priscilla@graceland.org') }

  before do
    allow(PaymentService).to receive(:stripe_customer).and_return(stripe_customer_api)
    allow(stripe_customer_api).to receive(:retrieve).and_return(stripe_customer)
  end

  describe "#sync_with_stripe" do
    before do
      user_with_card.as_null_object
      user_without_card.as_null_object
      allow(stripe_customer).to receive(:delete)
      allow(stripe_customer).to receive(:card=)
      allow(stripe_customer).to receive(:save)
    end

    context 'remove card from stripe' do
      before do 
        allow(user_with_card).to receive(:stripe_token).and_return(nil)
      end

      it 'should remove the stripe customer if the new token is nil' do
        expect(stripe_customer).to receive(:delete)
        PaymentService.sync_with_stripe(user_with_card)
      end
      
      it 'should reset the payment fields on the user' do
        expect(user_with_card).to receive(:reset_payment_fields)
        PaymentService.sync_with_stripe(user_with_card)
      end
    end

    context 'update existing card' do
      before do
        allow(user_with_card).to receive(:stripe_token).and_return('456')
      end

      it 'should update the stripe customer' do
        expect(stripe_customer).to receive(:card=)
        expect(stripe_customer).to receive(:save)
        PaymentService.sync_with_stripe(user_with_card)
      end

      it 'should update the user object' do
        expect(user_with_card).to receive(:stripe_customer_id=)
        PaymentService.sync_with_stripe(user_with_card)
      end
    end

    it 'should create a new stripe customer/card if adding a new one' do
      expect(stripe_customer_api).to receive(:create).and_return(stripe_customer)
      expect(user_without_card).to receive(:stripe_customer_id=)
      allow(user_without_card).to receive(:stripe_token).and_return('789')

      PaymentService.sync_with_stripe(user_without_card)      
    end
  end
end
