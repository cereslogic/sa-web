
require_relative '../../app/services/password_reset'
require_relative '../support/mock_mailers'

describe PasswordReset do
  let(:user) { double('User') }
  let(:mail) { double('Mail', deliver: true) }
  
  before do
    allow(UserMailer).to receive(:password_reset).and_return(mail)
    user.as_null_object
    mail.as_null_object
  end

  it 'should update the password reset token' do
    expect(user).to receive(:password_reset_token=)
    PasswordReset.new(user).send_password_reset   
  end

  it 'should update the password reset token' do
    expect(user).to receive(:password_reset_at=)
    PasswordReset.new(user).send_password_reset   
  end

  it 'should send an e-mail' do
    expect(UserMailer).to receive(:password_reset)
    expect(mail).to receive(:deliver) 
    PasswordReset.new(user).send_password_reset   
  end
end
