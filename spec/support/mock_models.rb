
class MockModel
  def initialize(opts={})
  end
end

unless defined?(User)
  class User < MockModel
  end
end

unless defined?(Payment)
  class Payment < MockModel
  end
end

unless defined?(Plan)
  class Plan < MockModel
  end
end

