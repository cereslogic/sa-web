FactoryGirl.define do
  factory :plan do
    default_at_signup true
    short_description "Default Plan"
    description "A Default plan"
  end
end
