Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Omniauth/Authorization stuff
  match "/auth/:provider/callback" => "sessions#create", via: [:get, :post]
  match "/auth/failure", to: "sessions#failure", via: [:get, :post]
  match "/login", to: "sessions#new", :as => "login", via: [:get]
  match "/logout", to: "sessions#destroy", :as => "logout", via: [:get, :post]  #TODO
  match '/register', to: "users#new", :as => "register", via: [:get]
  resources :users do
    member do
      resource :payment_source
    end
  end
  resource  :password
  resource  :password_reset
  resources :auth_tokens
  resources :payments

  resource :demo_result
  resources :users

  post 'address-query' => 'street_address#query', :as => :query_street_address

  # For the load balancer.
  get 'health_check' => 'health_check#index', :as => :health_check
end
