$redis_uri = URI.parse(ENV['REDIS_URL'] || 'redis://127.0.0.1:6379/0')
$redis = Redis.new(
  :host => $redis_uri.host, 
  :port => $redis_uri.port, 
  :password => $redis_uri.password
)

Redis::Objects.redis = ConnectionPool.new(size: 5, timeout: 5) { $redis }
