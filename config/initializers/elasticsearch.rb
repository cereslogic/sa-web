$repository = Elasticsearch::Persistence::Repository.new do
  # Configure the Elasticsearch client
  client Elasticsearch::Client.new url: ELASTICSEARCH_URL, log: true

  # Set a custom index name
  index "streets_#{Rails.env}"

  # Set a custom document type
  type  'address'

  # Specify the class to inicialize when deserializing documents
  klass Address
end
