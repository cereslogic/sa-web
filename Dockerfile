# FROM rails:onbuild
FROM ruby:2.2

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

ENV RACK_ENV production
ENV PORT 5200
ENV BUNDLE_JOBS=2

RUN mkdir -p /app
WORKDIR /app

ADD docker_entrypoint.sh /docker_entrypoint.sh
ADD Gemfile* /app/

RUN gem install --no-ri --no-rdoc bundler
RUN bundle install

ADD . /app

RUN apt-get update
RUN apt-get install -y nodejs --no-install-recommends
RUN apt-get install -y postgresql-client --no-install-recommends
RUN apt-get install -y nginx --no-install-recommends
RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
RUN sed "s/listen 5200/listen '\/tmp\/unicorn.sureaddress.sock'/g" /app/config/unicorn.rb > /app/config/unicorn_prod.rb
RUN mv /app/config/unicorn_prod.rb /app/config/unicorn.rb

# Add default nginx config
ADD nginx-sites.conf /etc/nginx/sites-enabled/default

EXPOSE 80

CMD /docker_entrypoint.sh
